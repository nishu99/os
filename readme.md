## This is my attempt at making an os.

## First steps:

1. When the power goes on, there are some checks that are performed called the POST process. Some other stuff happens too but as of now that is irrelevant.

2. The BIOS has a part of itself hard-coded onto the mother-board (ROM) and that part upon confirmation of proper power supply loads the BIOS onto some address on the RAM and transfers control to it.

3.  This now places an IVT onto the RAM ( ideally the first address itself because that's where the processor starts execution but hypothetically you could also have an instruction to do something (like say hi ;p) and subsequently go to the IVT).

4. The IVT is short for the Interrupt Vector Table. The indices here are interrupt numbers and the elements are various interrupt service routines. When an interrupt is fired, the cpu goes to the ISR(Interrut Service Routine) pointed by the element at the interrupt number index.

5. One of these interrupts is the guy who goes on the lookout for a bootable device to load the OS from. Now, what's a bootable device? .. Well, before that , we'll see this: so every device is divided into memory sectors. Now, there is data loaded into into these sectors. The one for which the first sector ends with the bytes 0xXXYY ( this is the boot signature) is our bootable drive. The first sector of this device is the bootloader. This bootloader code is loaded into the RAM and control is transferred to it. 

6. It'll now be the responsibility of this bootloader code to start the running of the rest of the Operating System.
